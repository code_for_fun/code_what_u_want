Le but de cet exercice est d'écrire un script pour encoder et décoder des 
fichiers.
Chacun fait comme il le sent mais on pourrait prendre le fichier au niveau 
binaire et l'encoder avec des fonctions bijectives telles que le XOR ou 
inverser les bits (changer les 0 en 1 et les 1 en 0) ou d'autres encore. 
Bref, à chacun de voir. Une proposition de reformulation de l'exercice 
ne serait pas dépréciée.