#Code for fun

Dans ce repo, il s'agira de s'inventer des exercices que nous nous amuserons à 
coder dans un ou plusieurs langages de notre choix. 

Lorsque vous avez des idées d'exercices, vous mettez le thème de l'exercice 
dans le fichier **exercices.md**, et vous créez un dossier pour ça auquel vous 
ajoutez éventuellement un fichier README. Un exemple est donné avec le 
**Hello_world**.