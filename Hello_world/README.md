#Introduction à la programmation
Il s'agit d'une minime introduction aux langages de programmation les plus 
répandus. Nous nous contenterons d'afficher un "Hello World" dans chacun de 
ces langages.

##Python
Pour exécuter ce code il vous faudra juste installer python3 si vous ne l'avez 
pas encore. Ensuite, vous vous placez dans le dossier **Python** et tapez dans 
votre terminal:
`python3 hello_world.py`
Bien évidemment, il y a diverses manières de faire mais dans ce document, nous 
nous contenterons de présenter les manières les plus fréquentes.

##C
Contrairement à ceux écrits en **Python**, les codes écrits en **C** doivent 
être compilés avant d'être exécutés. Un programmeur en C (comme dans pas mal 
d'autres langages) a besoin de :
* un éditeur de texte (c'est la base :-) );
* un compilateur (pour transformer le code en exécutable);
* et un déboggueur pour exécuter l'exécutable obtenu et faire ressortir 
d'éventuelles erreurs. 
Vous pouvez avoir les trois en un grâce à un IDE. Pour les langages C et C++, 
je vous recommande **CodeBlocks**. Vous n'avez donc qu'à y copier le code et 
à l'exécuter. Mais vous pouvez également vous passer d'IDE en installant 
**gcc** pour la compilation. 
Pour compiler et exécuter le code, ouvrez votre terminal et tapez:
* `gcc hello_world.c -o hello_world`
Cette commande génère un exécutable que nous avons nommé **hello_world**
Ensuite, pour exécuter c'est:
* `./hello_world`

##C++
Même procédé qu'avec le C à l'exception que pour compiler cette fois ci c'est 
**g++**.

##C\#
Ce [lien](http://www.lukemay.com/blog/2012/01/31/beginning-c-in-ubuntu-linux/) 
me semble assez intéressant pour savoir comment exécuter le code.

##JavaScript
Pour exécuter ce code, vous avez juste à ouvrir le fichier html avec un 
navigateur Web

##Java
Tout d'abord, il vous faudra installer un JDK(Java Development Kit).
Pour ceux qui sont sur Ubuntu,
* `sudo apt-get install openjdk-8-jdk`
Il se pourrait qu'au moment où vous lisez ce document, il y ait déjà une version plus récente, donc vérifiez si le coeur vous en dit.
Pour compiler, vous vous placez dans le dossier Java puis vous faites:
* `javac hello_world.java`
Un fichier .class sera généré, HelloWorld.class dans notre cas. Pour exécuter 
c'est :
* `java HelloWorld`


##Perl
* `perl hello_world.pl`
Et le tour est joué.